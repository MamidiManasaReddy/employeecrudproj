package com.antra.boot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.antra.boot.Model.EmployeeModel;
import com.antra.boot.service.EmployeeService;

@RestController
public class EmployeeController {
	
	@Autowired
	
	EmployeeService empServ;
	
	@PostMapping("/add")
	public String addEmployee(@RequestBody EmployeeModel emp) {
		return empServ.addEmployee(emp);
	}
	
	@GetMapping("/searchlist")
	public List<EmployeeModel> empList(){
		return empServ.searchEmps();
		
	}
	@PutMapping("/update")
	public String updateEmp(@RequestBody EmployeeModel emp) {
		return empServ.updateEmployee(emp);
	}
	@DeleteMapping("/delete/{id}")
	public String deleteEmp(@PathVariable int id) {
		return empServ.deleteEmps(id);
		
	}

}
