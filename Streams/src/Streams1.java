import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Streams1 {

	int id;

	String name;

	int age;

	String gender;

	String department;

	public Streams1(int id, String name, int age, String gender, String department) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.department = department;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Streams1 [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", department="
				+ department + "]";
	}

	public static void main(String[] args) {
		List<Streams1> employeeList = new ArrayList<Streams1>();

		employeeList.add(new Streams1(111, "Jiya Brein", 32, "Female", "HR"));
		employeeList.add(new Streams1(122, "Paul Niksui", 25, "Male", "Sales And Marketing"));
		employeeList.add(new Streams1(133, "Martin Theron", 29, "Male", "Infrastructure"));
		employeeList.add(new Streams1(144, "Murali Gowda", 28, "Male", "Product Development"));
		employeeList.add(new Streams1(155, "Nima Roy", 27, "Female", "HR"));
		employeeList.add(new Streams1(166, "Iqbal Hussain", 43, "Male", "Security And Transport"));
		employeeList.add(new Streams1(177, "Manu Sharma", 35, "Male", "Account And Finance"));
		employeeList.add(new Streams1(188, "Wang Liu", 31, "Male", "Product Development"));
		employeeList.add(new Streams1(199, "Amelia Zoe", 24, "Female", "Sales And Marketing"));
		employeeList.add(new Streams1(200, "Jaden Dough", 38, "Male", "Security And Transport"));
		employeeList.add(new Streams1(211, "Jasna Kaur", 27, "Female", "Infrastructure"));
		employeeList.add(new Streams1(222, "Nitin Joshi", 25, "Male", "Product Development"));
		employeeList.add(new Streams1(233, "Jyothi Reddy", 27, "Female", "Account And Finance"));
		employeeList.add(new Streams1(244, "Nicolus Den", 24, "Male", "Sales And Marketing"));
		employeeList.add(new Streams1(255, "Ali Baig", 23, "Male", "Infrastructure"));
		employeeList.add(new Streams1(266, "Sanvi Pandey", 26, "Female", "Product Development"));
		employeeList.add(new Streams1(277, "Anuj Chettiar", 31, "Male", "Product Development"));

		employeeList.stream().filter(e -> e.getAge() > 30).distinct().collect(Collectors.toList())
				.forEach(System.out::println);

		employeeList.stream().filter(e -> e.getDepartment() == "Sales And Marketing")
				.collect(Collectors.groupingBy(Streams1::getGender, Collectors.counting()))
				.forEach((String a, Long b) -> System.out.println(a + "=" + b));
		//Streams1 s=employeeList.stream().reduce(null, null)

	}

}
