package com.antra.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.antra.boot.Entity.Employee;
@Repository
public interface EmployeeRepository extends JpaRepository<Employee , Integer> {

}
