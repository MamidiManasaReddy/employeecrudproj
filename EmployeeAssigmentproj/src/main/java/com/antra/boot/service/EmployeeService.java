package com.antra.boot.service;

import java.util.List;

import com.antra.boot.Model.EmployeeModel;

public interface EmployeeService  {
	
	public String addEmployee(EmployeeModel empModel);
	public List<EmployeeModel> searchEmps();
	public String deleteEmps(Integer Id);
	public String updateEmployee(EmployeeModel empModel);
	
	
	
	

}
