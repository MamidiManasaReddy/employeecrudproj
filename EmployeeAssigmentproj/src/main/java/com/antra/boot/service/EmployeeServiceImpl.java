package com.antra.boot.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.boot.Entity.Employee;
import com.antra.boot.Model.EmployeeModel;
import com.antra.boot.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	EmployeeRepository empRepo;

	@Override
	public String addEmployee(EmployeeModel empModel) {
		
		boolean flag=empRepo.existsById(empModel.getEmpId());
		if(flag==false)
		{
			Employee e= new Employee();
			BeanUtils.copyProperties(empModel, e);
			empRepo.save(e);
			return "Employee saved";
		}
		else 
		{
			return "employee already exist";
		}
			
		
	}

	@Override
	public List<EmployeeModel> searchEmps() {
		
		List<Employee> empList=empRepo.findAll();
		List<EmployeeModel> empModelList=new ArrayList<>();
		empList.forEach(e-> {
			EmployeeModel emp=new EmployeeModel();
			BeanUtils.copyProperties(e, emp);
			empModelList.add(emp);
			
		});
		
		
		return empModelList;
	}

	@Override
	public String deleteEmps(Integer Id) {
		
		boolean flag=empRepo.existsById(Id);
		if(flag==true)
		{
			empRepo.deleteById(Id);
			
			return "Deleted Sucessfully";
		}
		else {
			return "employee does not exist with given Id";
		}
		
	}

	@Override
	public String updateEmployee(EmployeeModel empModel) {
		
		boolean flag=empRepo.existsById(empModel.getEmpId());
		if(flag==true) {
			Employee emp=new Employee();
			BeanUtils.copyProperties(empModel, emp);
			empRepo.save(emp);
			return "updated Sucessfully";
		
		}
		return "Employee id is not exist";
	}
	 
	

}
