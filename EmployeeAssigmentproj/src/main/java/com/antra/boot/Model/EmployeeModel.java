package com.antra.boot.Model;

public class EmployeeModel {
	private Integer empId;
	private String eName;
	private double salary;
	private String desg;
	
	
	public EmployeeModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public EmployeeModel(Integer empId, String eName, double salary, String desg) {
		super();
		this.empId = empId;
		this.eName = eName;
		this.salary = salary;
		this.desg = desg;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String geteName() {
		return eName;
	}
	public void seteName(String eName) {
		this.eName = eName;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getDesg() {
		return desg;
	}
	public void setDesg(String desg) {
		this.desg = desg;
	}
	@Override
	public String toString() {
		return "EmployeeModel [empId=" + empId + ", eName=" + eName + ", salary=" + salary + ", desg=" + desg + "]";
	}
	
	

}
