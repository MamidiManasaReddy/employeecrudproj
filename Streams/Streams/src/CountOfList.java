import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CountOfList {

	public static void main(String[] args) {
     /** List<Integer> myList=Arrays.asList(12,13,14,15,16,17,18);
      Long count=  myList.stream().count();
      System.out.println(count);
      
      List<Integer> myList1=Arrays.asList(10,15,8,49,25,98,98,32,15);
      myList1.stream().sorted().forEach(System.out::println);   **/
      
      List<Integer> myList2=Arrays.asList(10,15,8,49,25,98,98,32);
      myList2.stream().sorted(Collections.reverseOrder()).forEach(System.out::println);
      
      List<Integer> list = Arrays.asList(10, 12, 33, 45, 67);
		int sum = list.stream().mapToInt(i -> i.intValue()).sum();
		System.out.println(sum);

	}

}
